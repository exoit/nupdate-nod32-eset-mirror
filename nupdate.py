#!/usr/bin/python2.7

import json
import os
import sys
import time
import base64
import urllib
import urllib2
import ConfigParser
import random

class NUpdate:

    access = {}

    url = 'http://update.eset.com/eset_upd/update.ver'
    key = 'http://portal.d-market.com.ua/nod32keys/208-kljuchi-obnovlenija-nod32.html'
    user_agent = 'ESS Update \(Windows; U; 32bit; PVT F; VDB 21418; BPC 5.2.9.12; OS: 6.1.7600 SP 0.0 NT; TDB 21418; CL 0.0.0; LNG 1049; x64c; APP eav; BEO 1; ASP 0.10; FW 0.0; PX 0; PUA 1; RA 0\)'
    
    upd_rar = './update.rar'
    upd_ver = './update.ver'

    conf  = None
    host  = None
    links = []

    def __init__(self):
        string = open('./access.json', 'r+')
        access = json.load(string)

        self.access['login']    = access['login']
        self.access['password'] = access['password']

    def getUpdateVer(self):
        if os.path.exists(self.upd_rar):
            print 'Read old update.ver'
            self.__readUpdateVer()

            print 'Remove old update.ver'
            os.system('rm '+self.upd_ver)

            upd_ver = self.__selectServer()
            upd_ver = upd_ver+'update.ver'

            print 'Download native new update.ver'
            urllib.urlretrieve(upd_ver, self.upd_rar)
        else:
            print 'First download update.rar'
            urllib.urlretrieve(self.url, self.upd_rar)

        if not os.path.exists(self.upd_ver):
            print 'Unrar update.rar'
            os.system('unrar -xf '+self.upd_rar+' > /dev/null 2>&1')

        print 'Read update.ver'
        self.__readUpdateVer()

    def __readUpdateVer(self):
        conf = ConfigParser.ConfigParser()
        conf.read(self.upd_ver)

        self.conf = conf
        self.host = self.__selectServer().replace('/eset_upd/', '')

    def __selectServer(self):
        string = self.conf.get('HOSTS', 'Other')
        servers = string.split(', ')

        # Select random host
        host = servers[random.randint(0, len(servers)-1)].split('@')
        
        return host[1]

    def makeDownloadList(self):
        conf = self.conf
        
        for section in conf.sections():
            if section in ['HOSTS', 'Expire', 'SETUP']:
                continue

            for var, val in conf.items(section):
                if var in 'file':
                    f = val
                if var in 'size':
                    s = val

            self.links.append([self.host, f, s])

    def startDownload(self):
        auth = base64.encodestring('%s:%s' % (self.access['login'], self.access['password']))
        auth = auth.replace('\n', '')

        total_files    = len(self.links)
        download_files = 0
        passed_files   = 0
        cnt            = 0
        
        for domain, path, size in self.links:
            cnt+=1

            dfile = './dw'+path

            if os.path.exists(dfile):
                local_size = os.stat(dfile).st_size

                if int(local_size) == int(size):
                    passed_files+=1
                    continue

            try:
                request = urllib2.Request(domain+path)
                request.add_header('User-Agent', self.user_agent)
                request.add_header('Authorization', 'Basic %s' % auth)
                rf = urllib2.urlopen(request)

                download_files+=1
            except urllib2.HTTPError, err:
                #print err
                print 'Error, request: ' + str(err.code) + ' / ' + str(err.reason)
                sys.exit()
            except Exception:
                raise

            try:
                os.makedirs(os.path.dirname(dfile))
            except:
                pass

            try:
                total_size = rf.info().getheader('Content-Length').strip()
                total_size = int(total_size)
            except AttributeError:
                total_size = 0

            f = open(dfile, 'wb')

            bytes_so_far = 0

            while True:
                buffer = rf.read(1024)

                if not buffer:
                    sys.stdout.write('\n')
                    break

                bytes_so_far += len(buffer)
                f.write(buffer)

                percent = float(bytes_so_far) / total_size
                percent = round(percent*100, 2)
                sys.stdout.write('Download (%i/%i) %d of %d bytes (%0.2f%%)\r' % (cnt, total_files, bytes_so_far, total_size, percent))

        os.system('cp '+self.upd_rar+' ./dw/update.ver')

        print '   Total files: %i' % total_files
        print 'Download files: %i' % download_files
        print '  Passed files: %i' % passed_files

nu = NUpdate()
nu.getUpdateVer()
nu.makeDownloadList()
nu.startDownload()